package ex2


import ex2.model.Screen
import main.kotlin.ex1.model.AndroidDev
import main.kotlin.ex1.model.print

fun main(args: Array<String>) {

    /* Intro : This series of exercises are a continuation of andrew's exercises based on list. I'm reusing all the
     AndroidDevs, also a new class called "Screen" has been created, this class only contains the name of the screen
     (passenger,search etc..) and a list of developers who have worked on it.*/

    val andrew = AndroidDev("Andres", "Miguel", 35, 18)
    val jimmy = AndroidDev("Jaime", "Toca", 28, 36)
    val pablo = AndroidDev("Pablo", "Azaña", 30, 30)
    val dani = AndroidDev("Daniel", "Morales", 28, 18)
    val ana = AndroidDev("Ana", "Aguilar", 30, 12)
    val carlos = AndroidDev("Carlos", "Cordero", 31, 72)
    val victor = AndroidDev("Victor", "Legaz", 26, 6)
    val david = AndroidDev("David", "Torralbo", 33, 12)
    val nico = AndroidDev("Nicolás", "Gonzalez", 26, 11)
    val orozco = AndroidDev("Jose", "Orozco", 32, 10)
    val rave = AndroidDev("Raul", "Román", 26, 10)

    /* 1. Using the list screens (check below) show the developers that have work in passenger, payment and search
     * Note: The expected list should NOT contains repeated names neither list of lists.
       Expected output :
        Andres
        Carlos
        Daniel
        Jaime
        Ana
        Nicolás
        Raul
        David
        Victor */

    val passenger = Screen("Passenger", listOf(andrew, carlos, dani, jimmy))
    val payment = Screen("Payment", listOf(andrew, jimmy, carlos, ana, nico))
    val search = Screen("Seach", listOf(rave, david, victor))
    val screens = listOf(passenger, payment, search)

    println("The list of developers that have worked on Passenger, Payment and Search are")
    //your code here

    screens.flatMap { it.devs }.distinct().forEach { it.print() }

    /*----------------------------------------------------------------------------------------------------------------*/

    /* 2. Now, we want to have a list with 2 sublist. The first sublist should contain the developers that their index
     * in the below list androidDev is even and the second sublist the developers that their index in the list is odd.
     * Once you have a list like : ((ElementX, ElementY, ElementZ),(ElementA, ElementB, Element C)). Print all the names
     * Expected output:
       Raul
       Nicolás
       Victor
       Jose
       David
       Carlos
      */

    val androidDevs = listOf(rave, orozco, nico, david, victor, carlos)
    println("The two sublist are:")
    //your code here

    val resultado = androidDevs.partition { androidDevs.indexOf(it) % 2 == 0 }

    resultado.first.print()
    println()
    resultado.second.print()

    /*----------------------------------------------------------------------------------------------------------------*/

    /* 3. Can you tell the difference between
    *
    * this : androidDevs.map { it.age }.filter { it < 30 }
    *
    * and this: androidDevs.asSequence().map { it.age }.filter { it < 30 }.toList()
    *
    * Which is more efficient?
    * Why?
    * When would you use sequence and when list?
    * Answer the questions right here with comments */

    //your answer here

    /*----------------------------------------------------------------------------------------------------------------*/

    /* 4.Using the list screensWithRandomGuys
      Could you print the name of those developer whose age is less than 30, name is not empty and
      order it alphabetically ? */

    val randomGuy = AndroidDev("", "", 24, 35)
    val result = Screen("Result", listOf(rave, david, victor, randomGuy))
    val confimation = Screen("Confirmation", listOf(randomGuy, jimmy, andrew, ana, david))
    val screensWithRandomGuy = listOf(result, confimation)

    println("The list of the developers with less than 30, name non empty and ordered alphabetically is : ")
    //your code here

    // average ---
    // Usar Pair en alguno de los ejemplos
    screensWithRandomGuy.flatMap { it.devs }
        .filter { it.age < 30 && it.name.isNotEmpty() }.sortedBy { it.name }.print()


    /*----------------------------------------------------------------------------------------------------------------*/

    /* 5. Using screensWithRandomGuy Return a list of the form (screenName, average age of that devs screen)
     * Expected Output:
       Result, 27.25
       Confirmation, 30.0 */

    println("The list of the screens with the average age is: ")
    //your code here
    fun getAverage(name: String) =
        screensWithRandomGuy.filter { it.name == name }.flatMap { it.devs }.map { it.age }.average()

    val resultAverage = Pair(
        "${screensWithRandomGuy.first().name}, ${getAverage("Result")}",
        "${screensWithRandomGuy.last().name}, ${getAverage("Confirmation")} "
    )

    println(resultAverage.first)
    print(resultAverage.second)


}

