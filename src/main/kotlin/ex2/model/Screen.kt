package ex2.model


import main.kotlin.ex1.model.AndroidDev

class Screen(val name: String, val devs: List<AndroidDev>)