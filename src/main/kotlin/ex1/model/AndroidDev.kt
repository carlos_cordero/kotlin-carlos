package main.kotlin.ex1.model

class AndroidDev(val name: String, val surname: String, val age: Int, val monthsInCompany: Int) {

    val username: String get () = name + " " + surname

    override fun toString(): String {
        return "$surname, $name"
    }

    fun print() {
        println(toString())
    }
}

fun Collection<AndroidDev>.print() = forEach { println(it.name) }