package main.kotlin.ex1

import main.kotlin.ex1.model.AndroidDev
import main.kotlin.ex1.model.print

fun main(args: Array<String>) {

    val andrew = AndroidDev("Andres", "Miguel", 35, 18)
    val jimmy = AndroidDev("Jaime", "Toca", 28, 36)
    val pablo = AndroidDev("Pablo", "Azaña", 30, 30)
    val dani = AndroidDev("Daniel", "Morales", 28, 18)
    val ana = AndroidDev("Ana", "Aguilar", 30, 12)
    val carlos = AndroidDev("Carlos", "Cordero", 31, 72)
    val victor = AndroidDev("Victor", "Legaz", 26, 6)
    val david = AndroidDev("David", "Torralbo", 33, 12)
    val nico = AndroidDev("Nicolás", "Gonzalez", 26, 11)
    val orozco = AndroidDev("Jose", "Orozco", 32, 10)
    val rave = AndroidDev("Raul", "Román", 26, 10)
    val androidDevs = listOf(andrew, jimmy, pablo, dani, ana, carlos, victor, david, nico, orozco, rave)
    println()
    println("Show the two developers that have been here the longest sorted by the oldest")
    //CODE

    androidDevs.sortedWith(compareBy({ -it.monthsInCompany }, { -it.age })).take(2).print()

    println("Show the developer with less than 30 years that have been here the longest (saying, The youngest longest is:)")
    //CODE

    androidDevs
        .filter { it.age < 30 }
        .sortedBy { it.monthsInCompany }
        .takeLast(1).print()

    println()
    println("Show all developers of any age that have been here between 12 and 18 months keeping the current list order")
    //CODE
    androidDevs.filter { it.monthsInCompany in 12..18 }.print()

    println()
    println("Show all developers sorted by Surname, name")
    //CODE
    androidDevs.sortedWith(compareBy({ it.surname }, { it.name })).print()

    println()
    println("Show who is the dev with the longest name")
    //CODE
    androidDevs.maxBy { it.name.length }?.print()

}